; Copyright (C) 2014 Google Inc.
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;    http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns hesokuri.hesobase
  "Module for reading and writing the Hesokuri database. The database is a bare
Git repository that contains configuration and state information about the repos
synced by Hesokuri. The master branch is the configuration that is active on the
local machine. Merges are managed with Hesokuri-specific logic that is aware of
the semantics of the data.

Data is organized as a file system, rather than a single flat file. This makes
the database more storage-efficient when updated frequently.

The Hesobase has slightly different information from the original configuration
file, but the same kind of objects (source, peer) are present. Each object
is represented by a directory:

/              # repo root
/peer/         # contains all peers
/peer/{name}   # contains information on peer named {name}
/source/       # contains all sources (synced repos)
/source/{name} # contains information on source named {name}

{name} for source and peer is percent-encoded. The {name} of a peer corresponds
exactly with the address used to access the peer.

When {name}, {path}, or {branch-name} appear in a directory or file name, it is
percent-encoded.

FOR EACH PEER
-------------

In file called 'port'
A plain-text integer indicating the port on which the peer listens for Hesokuri
connections.

In file called 'key'
A Java-serialized instance of the RSA public key
(result of (.getPublic (hesokuri.ssh/new-key-pair)))

Files named 'source/{name}'
The presence of the file indicates that the peer has a copy of the source named
{name}. The contents of the file is a raw String indicating the path of the
source on the peer. If the path is relative, it is relative to hesoroot on that
peer.

FOR EACH SOURCE
---------------

Empty files called 'live-edit/only/{branch-name}' (optional)
Indicates a branch is considered a live edit branch. Any branch not listed here
IS NOT live edit.

Empty files called 'live-edit/except/{branch-name}' (optional)
List of branch names that are NOT considered live edit branches. Any branch not
listed here IS a live edit branch.

The live-edit/only and live-edit/except directories cannot both exist for a
single source.

Empty files called 'unwanted/{branch-name}/{hash}' (optional)
The presence of such a file tells Hesokuri to delete any branch with the given
name and SHA-1 hash. It will probably be very common for each branch-name to
only have a single SHA listed, but by allowing multiple SHAs, you can later
re-use the same branch name for newer work.

FUTURE IMPROVEMENTS
-------------------

The name of the peer and its address are the same thing. Allow multiple
addresses or allow the name to be mnemonic in cases where the address is an IP
or something arbitrary.)
***
There are some instances of 'empty files.' Each empty file may some day be
changed to a directory or a non-empty file to hold more information.
***
Merge conflicts that cannot be resolved automatically should be summarized in
some kind of log in the source, so in the off-chance it happens, the user can
recover."
  (:require [clojure.java.io :as cjio]
            [hesokuri.git :as git]
            [hesokuri.ssh :as ssh]
            [hesokuri.transact :as transact]
            [hesokuri.util :refer :all]))

(defn init
  "Initializes the hesobase repository with the information of a single peer.
  Returns the hash of the first commit.

  git-ctx - instance of hesokuri.git/Context
  machine-name - the name of the first peer.
  port - the port on which the first peer listens for Hesokuri connections.
  key - the key of the first peer. Before storing, this will be coerced with
      ssh/public-key-str.
  author - the author string of the first commit in the hesobase repo. See
      hesokuri.git/author."
  [git-ctx machine-name port key author]
  (git/throw-if-error (git/invoke-with-summary git-ctx "init" ["--bare"]))
  (let [tree (->> (git/add-blob ["peer" machine-name "port"] (str port))
                  (git/add-blob ["peer" machine-name "key"]
                                (ssh/public-key-str key)))
        commit-hash
        ,(git/write-commit git-ctx [["tree" nil tree]
                                    ["author" author]
                                    ["committer" author]
                                    [:msg "executing hesobase/init\n"]])]
    (git/throw-if-error
     (git/invoke-with-summary
      git-ctx "update-ref" ["refs/heads/master" (str commit-hash) ""]))
    commit-hash))

(defn tree->config
  "Converts a tree to the config format, which is defined by
  hesokuri.config/validation. The tree is in the format returned by
  hesokuri.git/read-tree with hesokuri.git/read-blob as the blob reader."
  [tree]
  (letfn [(reducer [config [path & blob-detail]]
            (case (path 0)
              "peer"
              ,(let [peer-name (%-decode (path 1))
                     blob-str (nth blob-detail 1)]
                 (case (path 2)
                   "port"
                   ,(assoc-in config
                              [:host-to-port peer-name]
                              (Integer/parseInt blob-str))
                   "key"
                   ,(assoc-in config
                              [:host-to-key peer-name]
                              (ssh/public-key blob-str))
                   "source"
                   ,(let [source-path (%-decode (path 3))]
                      (assoc-in config
                                [:source-name-map source-path :host-to-path
                                 peer-name]
                                blob-str))))
              "source"
              ,(let [source-name (%-decode (path 1))]
                 (case (path 2)
                   "live-edit"
                   ,(let [kind (keyword (path 3))
                          branch-name (%-decode (path 4))]
                      (conj-in config
                               [:source-name-map source-name :live-edit-branches
                                kind]
                               branch-name
                               #{}))
                   "unwanted"
                   ,(let [branch-name (%-decode (path 3))
                          branch-hash (path 4)]
                      (conj-in config
                               [:source-name-map source-name :unwanted-branches
                                branch-name]
                               branch-hash
                               []))))))
          (add-sources-vector [config]
            (assoc config :sources (vec (vals (:source-name-map config)))))]
    (->> (git/blobs tree)
         (reduce reducer {:source-name-map (sorted-map)})
         add-sources-vector)))
